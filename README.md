# modemctl-cm1150v

Shell script to query and control Netgear CM1150V web interface via command line

## Usage

`modemctl command [--debug] [--username=] [--password=] [--no-prompt]`

### Commands:
- connection_status / cs: Report if the connection status is OK
- downstream_status / ds / errors: Report OFDM Downstream channel status
- reboot: Reboot the modem
- status: Get up / down status of modem
- wait_for_connection / wfc: Wait for connection status to become OK (useful after reboot)

### Optional Arguments
`--username / --password:`

  Username and password can also be set as environment variables MODEMCTL_USERNAME / MODEMCTL_PASSWORD.  If not 
  set or provided, will be prompted when the command is run.

`--no-prompt`

  If running in automated script / non-interactive mode, skip any prompts.  If username / password not provided will simply fail.
